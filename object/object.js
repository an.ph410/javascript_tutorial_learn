console.log("Có 2 cách khai báo:");
console.log("object literal: {}");
console.log("object constructor: new Object()");

const student = {
  name: "an",
  "last-name": "pham",
  age: 22,
  female: true,
  hi: function () {
    console.log("Hello bạn");
  },
};

const student01 = {
  class: "A01",
  female: false,
};

console.log(student);

console.log("2 cách truy xuất giá trị");
console.log(
  `Dot notation - student.name: ${student.name}`,
  `\nBracker notation ["key"] - student["last-name"] :${student["last-name"]}`
);

console.log("Thay đổi giá trị");
student.age = 20;
student["age"] = 21;
student.isDeveloper = true;
student["is-developer"] = false;
student.hello = function () {
  console.log("Hello");
};
console.log(student);

console.log('Xóa giá trị: delete student["is-developer"]');
delete student["is-developer"];
console.log(student);

console.log("for...in trong object");

for (let key in student) {
  console.log(`${key} => ${student[key]}`);
}

console.log("Object.keys => trả về 1 mảng chứa tất cả key của object");
console.log(Object.keys(student));
console.log("Object.values => trả về 1 mảng chứa tất cả giá trị của object");
console.log(Object.values(student));
console.log(
  "Object.entries => trả về 1 mảng có nhiều mảng con, với mỗi mảng con gồm 2 phần tử, 1 là key, 2 là value"
);
console.log(Object.entries(student));

console.log("Object.assign => clone object, gộp object");
let newStudent = Object.assign(student, student01);
let newStudent01 = { ...student, ...student01 };
console.log(newStudent, newStudent01);
console.log(
  "Object.freeze => ngăn chặn việc chỉnh sửa key và value của object"
);

const car = {
  brand: "BMW",
};
let newCar = Object.freeze(car);
car.age = 22;
console.log(newCar);
console.log("Object.seal => ko cho phép thêm, được chỉnh sửa");

const user = {
  name: "evondev",
};

let newUser = Object.seal(user);
newUser.price = 230000;
newUser.name = "An Pham";
console.log(newUser);

const customer = {
  userName: "An Pham",
  company: {
    name: "Wind",
    room: {
      name: "IT",
    },
  },
};

console.log(
  "Chỉ truy xuất được 1 cấp, dễ bị ảnh hưởng do nested object phức tạp"
);
let newCustomer = { ...customer };
let newCustomer1 = Object.assign({}, customer);

console.log(newCustomer);
console.log(newCustomer1);

console.log("giải pháp dùng stringify và json parse");
let newCustomer2 = JSON.parse(JSON.stringify(customer));
newCustomer2.company.name = "Sun";
console.log(newCustomer2);

console.log("Từ khóa this trong object: đề cập tới object gần nhất");

const student02 = {
  name: "an",
  "last-name": "pham",
  age: 22,
  female: true,
  hi: function () {
    console.log(`Hello ${this.name} ${this["last-name"]}`);
  },
  //   fullName: {
  //     name: "Pham Hong An"
  //   }
};

student02.hi();

console.log("Optional Chaining => student02.fullName?.name?");
console.log(student02.fullName?.name);

console.log("Destructuring object");

const { name, age, female, ...rest } = student02;
console.log(name, age, female, rest);

function whatYourInfo(obj) {
  console.log(obj.name, obj.age, obj.school);
}

function whatYourInfo2({ name, age, school }) {
  console.log(name, age, school);
}

const newObj = {
  name: "Pham An",
  age: 18,
  school: "Open University",
};

whatYourInfo(newObj);
whatYourInfo2(newObj);

console.log("1/ Kiểm tra value có phải là object hay không");
// typeof value === "object"
// {} [] null

function isObject(value) {
  if (typeof value === "object" && !Array.isArray(value) && Boolean(value)) {
    return true;
  }
  return false;
}

const arrCheck = [{}, [], null];
arrCheck.forEach((ele) => console.log(isObject(ele)));

console.log('2/ {a: 1, b: 2} => [["a", 1], ["b": 2]]');
function objectToArray(object) {
  if (!isObject(object)) return;
  //   const value = Object.keys(object).map((key) => [key, object[key]]);
  let result = [];
  for (let key in object) {
    if (object.hasOwnProperty(key)) {
      result.push([key, object[key]]);
    }
  }

  console.log("hasOwnProperty", result);
  return Object.entries(object);
}

console.log(objectToArray({ a: 1, b: 2 }));

console.log("3/");

function without(object, ...key) {
  console.log(key);
  let result = [];
  for (let keyObj in object) {
    console.log(key.hasOwnProperty(keyObj));
    if (!key.hasOwnProperty(keyObj)) {
      result.push([keyObj, object[keyObj]]);
    }
  }

  return result;
}

console.log(without({ a: 1, b: 2, c: 3, d: 8, f: 3 }, "b", "c", "f"));

console.log("4/ function kiểm tra 2 object truyền vào có bằng nhau hay không");

function isEqualObject(obj1, obj2) {
  const objKey1 = Object.keys(obj1);
  const objKey2 = Object.keys(obj2);

  if (objKey1.length !== objKey2.length) return false;

  const result = objKey1.every((key) => obj1[key] === obj2[key]);
  return result;
}

console.log(isEqualObject({ a: 1 }, { a: 1, b: 2 }));
