console.log(5 + 7);
console.log(typeof 10);

const number1 = "1.8";
const number2 = "5";

console.log(parseInt(number2));
console.log(parseFloat(number1));

const number3 = -10;
// Math.abs(value): gia tri tuyet doi
console.log(Math.abs(number3));
// Math.floor(value): lam tron xuong 4.3 -> 4
console.log(Math.floor(4.3));

// Math.ceil(value): lam tron len 4.3 -> 5
console.log(Math.ceil(4.3));

// Math.round(value): lam tron gan nhat
console.log(Math.round(4.3)); //4
console.log(Math.round(4.6)); //5

// toFixed(number) -> return string
console.log(parseFloat((2.44444444444444444).toFixed(2))); //2.44

console.log(Math.random()); //[0-1]
console.log(Math.ceil(Math.random() * 10)); //[0-10]

console.log(Math.max(1, 3, -5, -7, 100));
console.log(Math.min(1, 3, -5, -7, 100));
console.log(Math.pow(3, 2)); // 3^2=9

console.log(isNaN(true));
// isNaN(value) vs Number.isNaN(value)
// NaN: not a number 

console.log('isNaN', isNaN("object")); //true
console.log('isNaN', isNaN("123")); //false

const isNaNF = function(value) {
    const n = Number(value);
    return n !== n;
}

// Number.isNaN: not a number but type is number

console.log('Number.isNaN', Number.isNaN("object")); //false 
console.log('Number.isNaN', Number.isNaN("123")); //false
console.log('Number.isNaN', Number.isNaN(0)); //true