// String(Chuỗi)

// Template literal: double quote, backticks, single quote

const name = "An";

console.log(typeof name);

console.log(`Hello${name}`);
// index là vị trí của từng kí tự bắt đầu từ 0

const myString = "Frontend Developer";
console.log(myString.split(" "));
// ['Frontend', 'Developer']
console.log(myString.split(""));
// ['F', 'r', 'o', 'n', 't', 'e', 'n', 'd', ' ', 'D', 'e', 'v', 'e', 'l', 'o', 'p', 'e', 'r']
console.log(myString.split("/"));
// ['Frontend Developer']

console.log(myString.toLowerCase());
// in thường

console.log(myString.toUpperCase());
// in hoa

console.log(myString.startsWith("Frontend"));
console.log(myString.endsWith("F"));
console.log(myString.includes("end"));
// return true or false

console.log(myString.indexOf("3"));
console.log(myString.lastIndexOf("e"));
// return index or -1 if not found

console.log(myString.replace("Frontend", "FE"));
console.log(myString.repeat(5));

// slice: create a new string and slice character u want
console.log(myString.slice(0, 8));
console.log(myString.slice(0)); //Frontend Developer
console.log(myString.slice(-5)); //loper
console.log(myString.slice(999)); //khong print gi

// trim: loai bo khoang trong giua hai ben

const strSpace = "   FR Dev   ";
console.log(strSpace.trim()); // remove space left and right
console.log(strSpace.trimStart()); // remove space left
console.log(strSpace.trimEnd());

console.log(strSpace.charAt(8));

// substr(index, length): lấy ra 1 phần của chuỗi
// substring(indexStart, indexEnd): lấy ra các ký tự của chuỗi

const myStr = "FE Dev -1";
console.log(myStr.substr(1, 5)); //E Dev

console.log(myStr.substring(1, 5)); //E De

const myStr3 = "        Frontend Developer D    ";
// Loại bỏ khoảng trống 2 bên, đưa về in hoat, thay Developer D -> Developer and Designer, lặp lại 2 lần
console.log(
  myStr3
    .trim()
    .replace("Developer D", "Developer and Designer")
    .toUpperCase()
    .repeat(2)
);
