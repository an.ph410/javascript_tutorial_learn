// 1. Viết function so sánh 2 số a, b tìm ra số lớn hơn

function compareNumbers(a = 0, b = 0) {
  if (typeof a !== "number" || typeof b !== "number") {
    console.log("Please input valid number!");
    return;
  }
  //   return a > b ? a : b;
  return Math.max(a, b);
}

console.log(compareNumbers(2, 5));

// 2. In hoa chữ cái đầu trong ví dụ

function converse(string) {
  if (!string.length) return null;
  string = string.toLowerCase();

  return string.charAt(0).toUpperCase() + string.slice(1);
}

// let inputName = prompt("Plese input your string: ")
let inputName = "nGuhds";
// console.log(converse(inputName));

// 3. Viết func sử dụng callback (func1 là parameter của func khác) -> in ra kết quả của hàm compare đã viết ở trên

function useCallbackCompare(a, b, fn) {
  return fn(compareNumbers(a, b));
}

function printResult (number) {
    console.log(`Max number is ${number}`);
}

useCallbackCompare(4, 5, printResult);

// 4. Viết function tính bình phương 1 số

const numberSqure = (number) => Math.sqrt(number);
console.log(numberSqure(8));

const handleCentury = (number) => {
    let numberRound = number % 100;
    console.log(numberRound);
    if (numberRound === 0) {
        return `The ky: ${Math.floor(number / 100)}`
    }
    return `The ky: ${Math.floor(number / 100) + 1}`
}

console.log(handleCentury(2019));

// 5.  Cho 1 chuỗi dài hơn 15 ký tự. Viết 1 function cắt chuỗi, lấy ra 10 ký tự đầu tiên và thêm vào dấu "..." ở cuối chuỗi.

function cutString (string) {
    if (string.length > 15) {
        return `${string.slice(0, 15)}...`
    }
    return string;
}

console.log(cutString("Tiền nhiều để làm gì~"));

// 6. Viết 1 function lấy ra 1 số nhỏ nhất trong 1 mảng các số.

function minNumber (arr) {
    return arr.reduce((accumulator, element) => {
        console.log(accumulator, element);
        return accumulator < element ? accumulator : element;
    })
}

console.log(minNumber([5, 4, 7, 2, 8, 7, 3]));

function arrangeArray (arr) {
    return arr.sort((a,b) => {
        return a - b;
    })
}
console.log(arrangeArray([5, 4, 7, 2, 8, 7, 3]));

