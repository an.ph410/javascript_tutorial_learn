// Funtion tong 2 so
function sum(a = 0, b = 0) {
  return a + b;
}

console.log(sum(12, 3));

// Tham so la function
function pow(a, b, fn) {
  return Math.pow(fn(a, b), 2);
}

let resultPow = pow(200, 300, sum);
console.log(resultPow);
// Luu function vao 1 variable
const sum2 = sum;
function average(a, b, fn) {
  return fn(a, b) / 2;
}

let result = average(200, 300, sum2);
console.log(result);

// Function declaration : hoisting
logName();
function logName() {
  console.log("Function declaration");
}

// Anonymous function (function expression): khong bi hoisting
const logName1 = function () {
  console.log("your name");
};

console.log(logName1);

// IIFE -> immediately invoked function excution: chay ngay lap tuc
(function () {
  console.log("IIFE");
})();

// Scope: pham vi bien co the truy cap
// Global scope:
let myName = "a"; //globle scope
function logYourName() {
  let myName2 = "a"; // function scope
  console.log(myName2);
}

logYourName();

// block scope
if (2 > 1) {
  let message2 = "a";
  // let, const: block
  // var: global scope & hoisted
}

// Closure: là nhiều function được lồng vào nhau, cho phép truy xuất function bên trong ra function bên ngoài. Nó sẽ được khởi tạo mỗi khi function được tạo

// Lexical scope: định nghĩa scope của biến
let aNewName = "Evondev"; //global scope
function sayHello() {
  let message5 = "Hi"; //block scope, function scope, local scope
  console.log(`${message5} ${aNewName}`);
}

function sayHi() {
  //parent func
  let message1 = "Hi";
  function sayHi2() {
    //child func
    console.log(message1);
  }
  return sayHi2();
}

let hello = sayHi();
hello;

function sayHello3(message) {
  return function sayHi3(name) {
    console.log(`${message} ${name}`);
  };
}

let hello3 = sayHello3("Welcome to javascript");
hello3(" Closure ");

function anotherFunction() {
  let otherMessage = "hello 01";
  function sayHi() {
    console.log(otherMessage);
  }
  return sayHi();
}

let callFunc = anotherFunction(); // otherMessage is no longer accessible
callFunc;

// arrow function: anonymous function (func không có tên) và không bị hoisting

// Normal function
const square = function (x) {
  return x * x;
};

// arrow function

const sumFunc = (x) => x * x;

console.log('square',square(5));
console.log('a',sumFunc(5));