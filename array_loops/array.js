// array literal
// const student1 = [];
// array constructor
// const student2 = new Array();

const student = ["evondev", "john", "mary", "anna", "elsa", "john"];
// Phương thức hay dùng
// length: chiều dài của mảng
console.log(student.length);

// reverse: đảo ngược mảng
console.log("reverse:", student.reverse());

// join: nối các phần tử trong mảng
console.log("join: ", student.join());
console.log("join: ", student.join(" "));

// includes: kiểm tra mảng đó cos chừa phần tử nào đó không?
console.log("includes:", student.includes("anna"));

// indexOf: trả về vị trí của phần tử tìm thấy đầu tiên
console.log("indexOf:", student.indexOf("john"));

// lastIndexOf
console.log("lastIndexOf:", student.lastIndexOf("john"));

// push: thêm phần tử vào cuối
student.push("techcraft");
console.log("push", student);
// unshift: thêm vào đầu
student.unshift("techcraft");
console.log("unshift", student);
// pop: xóa cuối
student.pop();
console.log("pop", student);
// unshift: xóa đầu
student.shift();
console.log("shift", student);

console.log("-----------------Sile-----------------------");
const animals = ["tiger", "dog", "cat", "bird", "elephant"];

// slice: tạo ra 1 mảng coppy của mảng ban đầu

// slice(start, end) :=> start là vị trí index ở trong mảng, không lấy end [)
const animals2 = animals.slice();
console.log(animals2);
console.log(animals2.slice(1, 2));
console.log(animals2.slice(-2));

console.log("-----------------Spile-----------------------");
// Xóa phần tử trong mảng hoặc thay thế
const animals3 = [...animals];
console.log("ban dau:", animals3);

animals3.splice(1, 2, "add1", "add2", "add3", false, 100);

console.log("splice:", animals3);

console.log(
  "-----------------Sắp xép các phần tử trong mảng theo chuẩn unicode-16-----------------"
);
const random = [1, 9990, 22, 10, 5, 34, 7];
const random2 = [...random];
console.log(random.sort((a, b) => a - b));
console.log(random.sort((a, b) => b - a));

const random3 = random.sort(function (a, b) {
  if (a > b) return 1; // tăng dần
  if (a < b) return -1; // giảm dần
});

const random4 = random.sort((a, b) => (a > b ? 1 : -1));
const random5 = random2.sort((a, b) => (a > b ? -1 : 1));

console.log("random3: ", random3);
console.log("ternary operator: ", random4);
console.log("ternary operator: ", random5);

console.log(
  "-----------------Find: trả về phần tử đầu tiên tìm thấy trong mảng-----------------------"
);
console.log(random.find((value) => value > 10));
// undefined nếu không tìm thấy

console.log(
  "------findIndex: trả về vị trí index của phần tử đầu tiên tìm thấy trong mảng--------"
);
console.log(random.findIndex((value) => value > 10));
// -1 nếu không tìm thấy

console.log(
  "array.map: duyệt phần tử trong mảng và return 1 mảng mới, don't affect previous array"
);

const lstNumber = [1, 2, 3, 4, 5];
console.log("mảng ban đầu: ", lstNumber);

const lstNumberDouble = lstNumber.map((ele) => ele * 2);
console.log(lstNumberDouble);

console.log(
  "array.forEach: duyệt phần tử trong mảng, affect previous array, ko có return, ko bao giờ dừng được"
);

console.log(
  "--Filter: trả về các phần tử thỏa điều kiện xét kèm trong mảng. trả về mảng mới -----"
);

const greaterThanThree = lstNumber.filter((num) => num > 3);
console.log(greaterThanThree);
console.log(lstNumber.filter((num) => num > 30));

console.log(
  "--Some: trong mảng có chứa ít nhất 1 giá trị thỏa điều kiện trong callback function-----"
);

console.log(lstNumber.some((value) => value > 2));

console.log(
  "--Every: tất cả giá trị trong mảng phải thỏa điều kiện trong callback function-----"
);
console.log(lstNumber.every((value) => value > 2));

console.log("--Reduce: gom các phần tử trong mảng về 1 giá trị-----");

// .reduce((a, b) => {}, initialize value)

const totalNumber = lstNumber.reduce(
  (prevNumber, currentNumber) => prevNumber + currentNumber,
  0
);

console.log(lstNumber, "sum =", totalNumber);

console.log("====================BAI TAP=====================");

console.log("Bài 1/ Đảo ngược 1 chuỗi");

function reverseString(str = "") {
  if (!str) return null;
  const newString = str.split(" ");
  return newString.reverse().join(" ");
}

console.log(reverseString("My name is An"));

console.log("Bài 2/ Đảo ngược 1 chuỗi bao gồm cả ký tự");

function reverseWord(str) {
  if (!str) return null;
  const newString = str.split("");
  return newString.reverse().join("");
}

function reverseWord02(str) {
  if (!str) return null;
  const newString = str
    .split(" ")
    .map((ele) => ele.split("").reverse().join("")); //  ['i', 'evol']
  return newString.reverse().join(" ");
}

console.log(reverseWord("i love"));
console.log(reverseWord02("i love"));

console.log("Bài 3/ In hoa chữ cái đầu trong chuỗi");

function capitalizeStr(str) {
  if (!str) return null;
  const arrStr = str.toLowerCase().split(" ");
  const newArr = arrStr.map(
    (ele) => ele.charAt(0).toUpperCase() + ele.slice(1)
  );
  return newArr.join(" ");
}

console.log(capitalizeStr("my nAme iS aN"));

console.log("===========NEW============");
console.log("===========By value vs By referrences============");
console.log("By value: giá trị thực được lưu trong vùng bộ nhớ");

const number1 = 1;
const number2 = 1;
console.log(number1 === number2);
console.log("By referrences: nói tới vùng bộ nhớ");

const arr1 = [1, 2, 3];
const arr2 = [1, 2, 3];
console.log(arr1 === arr2);

console.log(
  "===========Cách so sánh 2 mảng cơ bản: JSON.stringify và JSON.parse============"
);

console.log("JSON: Javascipt Object Notation");
console.log("JSON.stringify: convert giá trị sang dạng JSON string");

console.log("toString():", arr1.toString());
console.log("JSON.stringify():", JSON.stringify(arr1));

console.log("JSON.parse: covert string sang dạng ban đầu");
console.log("JSON.parse:", JSON.parse(JSON.stringify(arr1)));

function compareArray(arr1, arr2) {
  return JSON.stringify(arr1) === JSON.stringify(arr2);
}

console.log(compareArray(arr1, arr2));

console.log("===========2 cách để sao chép mảng============");

const students = ["a", "b", "c", "d", "e"];

const students02 = students.slice();
const students03 = [...students];

console.log("1. Sử dụng slice", students02);
console.log("1. Sử dụng spread operator [...array]", students03);

console.log("===========2 cách để gộp mảng============");

const animals01 = ["a", "b", "c", "d", "e"];
const animals04 = ["f", "g", "h", "t", "k"];

const animals02 = animals01.concat(animals04);
const animals03 = [...animals01, ...animals04];

console.log("1. Sử dụng concat\n", animals02);
console.log("1. Sử dụng spread operator [...array1, ...array2]\n", animals03);

console.log("===========DESTRUCTURING ARRAY============");

const toys = ["ball", "sword", "arrow", "magic", "water", "fire"];
const [a, b, c, ...rest] = toys;
console.log(a);
console.log(rest);

console.log("===========REST PARAMETER============");

const newToys = ["ball", "sword", 100, 200, 300, 1];
const [pro1, pro2, ...otherPrice] = newToys;

function totalBill(product1, product2, ...price) {
  console.log(`Name Products: ${product1}, ${product2}`);
  const sum = price[0].reduce((prev, current) => prev + current);
  console.log(`Total Price: $${sum}`);
}

totalBill(pro1, pro2, otherPrice);

console.log("===========LOOPS============");

let sumRandom = 0;
for (let i = 0; i < random.length; i++) {
  sumRandom += random[i];
}

console.log(sumRandom);

for (let i = random.length - 1; i >= 0; i--) {
  console.log(random[i]);
}

console.log("1/ sao chép mảng dùng vòng lặp for");

let cloneAnimals = [];

for (let i = 0; i < animals.length; i++) {
  cloneAnimals.push(animals[i]);
}

console.log(cloneAnimals);

console.log("2/ Đảo ngược chuoix");

let str = "i love";
let arrReverse = [];

for (let i = str.length; i >= 0; i--) {
  arrReverse.push(str[i]);
}

console.log(arrReverse.join(""));

console.log("===========WHILE và DO WHILE, for of============");

// for (variable of array) {}

for (let n of animals) {
  console.log(n);
}

console.log("===========BÀI TẬP============");

console.log(
  "1/ Cho 1 mảng, viết function loại bỏ các giá trị là falsy ra khỏi mảng và chỉ giữ lại truthy."
);

// Falsy: undefined, false, 0, null, "", NaN

const arrEx01 = [
  1,
  1000,
  false,
  null,
  "evondev",
  "",
  undefined,
  "javascript",
  [1, 2, 3],
];

console.log(`Mảng ban đầu: ${arrEx01}`);

function removeValueFalsy(arr) {
  return arr.filter((ele) => Boolean(ele));
}

console.log(removeValueFalsy(arrEx01));

console.log(
  '2/ Cho mảng phức tạp bên dưới, hay in ra kết quả mong muốn như sau: \n [1,2,3,false,null,1,5,6,"javascript", 888, 6666, 90]'
);

const arrEx02 = [
  [1, 2, 3, [false, null]],
  [1, 5, 6, ["javascript"]],
  [888, 666, [90]],
];

let result = [];
function addUpArray(arr) {
  for (let x of arr) {
    for (let y of x) {
      if (Array.isArray(y)) {
        y.forEach((ele) => result.push(ele));
      } else {
        result.push(y);
      }
    }
  }

  console.log(result);
}

addUpArray(arrEx02);

console.log("Dùng flat(number)");
console.log(arrEx02.flat(2));

console.log("3/ Đảo ngược số nguyên");

const arrNumber = [1234, 4321, -567, -347];

arrNumber.forEach((num) => {
  console.log(`${num} => ${reverseNumber(num)}`);
});

function reverseNumber(number) {
  number = number.toString();
  let value = number.split("").reverse().join("");
  return parseInt(value) * Math.sign(number);
}

console.log(
  "4/ Viết chương trình có tên là fizzBuzz với đầu vào là một số nguyên, và chạy từ 1 cho tới số nguyên đó, rồi check nếu số chia hết cho 2 thì in \"Fizz\", nếu chia hết cho 3 thì i ra 'Buzz', nếu chia hết cho cả 2 số thì in ra 'FizzBuzz' "
);

function fixxBuzz(num) {
  if (!num) return null;

  for (let i = 1; i <= num; i++) {
    if (i % 2 === 0 && i % 3 === 0) {
      console.log("FizzBuzz");
    } else if (i % 2 === 0) {
      console.log("Fizz");
    } else if (i % 3 === 0) {
      console.log("Buzz");
    } else {
      console.log(i);
    }
  }
}
fixxBuzz(15);

console.log(
  "5/ Cho chuỗi bất kỳ, đếm số lượng kí tự `vowels`: u e o a i có trong chuỗi"
);

let strWordCompare = "ueoai";
function wordFrequence(str = "") {
  let convertStr = str.split("");
  let counter = 0;
  for (let value of strWordCompare) {
    counter += convertStr.filter((item) => item === value).length;
  }

  console.log(`${str} => ${counter}`);
}

wordFrequence("evondev");

console.log("6/ Viết function trả về mảng với các giá trị unique");

function uniqueNumber(arr) {
  let arrUnique = [];
  if (!Array.isArray(arr)) return [];
  for (let num of arr) {
    if (!arrUnique.includes(num)) {
      arrUnique.push(num);
    }
  }

  return arrUnique;
}

console.log(uniqueNumber([1, 2, 3, 1, 1, 1, 2, 2, 2, 5, 5, 5, 7, 7, 6]));

console.log(
  "7/ Viết function xử lý 1 mảng lớn thành nhiểu arr con dựa vào 1 số nguyên đầu vào"
);

function subdivisionOfShare(arr, number) {
  let newArray = [];
  for (let i = 0; i < arr.length; i += number) {
    newArray.push(arr.slice(i, i + number));
  }

  return newArray;
}

console.log(subdivisionOfShare([1, 2, 3, 4, 5, 7, 8, 9, 11, 22, 7], 3));
