const now = new Date();
// built-in object
console.log(now);
// Wed Oct 05 2022 19:30:10 GMT+0700 (Indochina Time)
// Timezone (múi giờ): GMT+0700 (Indochina time)

// Timestamp và epochtime
// epochtime: dựa trên unix time (từ ngày 1.1.1970 12g khuya)
console.log(now.getTime()); // print timestamp
console.log(new Date(0));

// new Date() -> in ra ngày giờ hiện tại
// new Date(timestamp) -> dựa vào timestamp để in ra ngày giờ
// new Date(date string)
// new Date(year, month, day, hours, minutes, seconds, miliseconds)

// Các hàm get trong Date
const birthday = new Date(2000, 10, 4, 16, 0, 0, 0);
// console.log(birthday.getFullYear());
// console.log(birthday.getMonth()); // 0 - 11
// console.log(birthday.getDate()); // 1- 31
// console.log(birthday.getDay()); // 0: chu nhat- 6
// console.log(birthday.getHours());
// console.log(birthday.getMinutes());
// console.log(birthday.getMilliseconds());
// console.log(birthday.getSeconds());
// console.log(birthday.getTime());

// Các hàm set trong Date
// Tìm hiểu UTC trong Date

console.log("new", now.toTimeString()); //20:06:11 GMT+0700 (Indochina Time)
console.log(now.toDateString()); //Wed Oct 05 2022
console.log(now.toLocaleDateString()); //10/5/2022
console.log(now.toLocaleDateString("vi-VI")); //5/10/2022
console.log(now.toISOString()); //2022-10-05T13:08:05.256Z

// Bài tập
// setTimeout & setInterval
// setTimeout: set 1 khoảng thời gian nhất định, sau bao lâu sẽ làm cái việc đó

const timeout = setTimeout(function () {
  console.log("call me after 3 seconds");
}, 3000);
clearTimeout(timeout);

const timer = setInterval(function () {
  console.log("call me"); // sau 1 giây thì console.log 1 lần
}, 1000);

clearInterval(timer);

// Bài tập chương 5
// 1. Viết chương trình nhập vào năm sinh và in ra số tuổi
function handleYourAge(yearOfBirth = 2000) {
  const currentYear = new Date().getFullYear();
  return `Your age: ${currentYear - yearOfBirth}`;
}

console.log(handleYourAge(1998));

// 2. Viết ctrinh đếm ngược thời gian từng giây (countdown) dựa vào thời gian đầu vào. Ví dụ: 30p hết thời gian làm bài

function countDownDoExam(minutes) {
  let seconds = minutes * 60;
  let counter = 0;
  const timer1 = setInterval(function () {
    counter++;
    console.log(counter);
    if (counter === seconds) {
      clearInterval(timer1);
      console.log("Your time is end!");
    }
  }, 1000);
  //   setTimeout(function () {
  //     console.log("Đã hết thời gian làm bài!");
  //   }, minutes);
}

countDownDoExam(0.1);

// 3. Viết ctrinh có tên là timeSince, đầu vào là thời gian và tính thời gian đầu vào so với thời gian hiện tại.
// `3 phút trước`, `3 ngày trước`, `2 tháng trước`, `30 giây trước`, `1 năm trước`
// 1 năm = 365 * 24 * 60 * 60 = 31536000
// 1 tháng = 31 * 24 * 60 * 60 = 2678400
// 1 tuần = 7 * 24 * 60 * 60 = 604800
// 1 ngày = 1 * 24 * 60 * 60 = 86400
// 1 giờ = 1 * 60 * 60 = 3600
// 1 phút = 1 * 60 = 60

let yourFriendTime = new Date(
  "Sun May 16 2021 00:30:10 GMT+0700 (Indochina Time)"
);

function timeSince(yourFriendTime) {
  let currentTime = new Date();
  let seconds =
    Math.floor(currentTime.getTime() - yourFriendTime.getTime()) / 1000; // mili -> seconds

  if (seconds < 0) {
    console.log("your time is invalid");
    return;
  }

  function handleTimer(seconds, unit) {
    return seconds / unit;
  }

  if (handleTimer(seconds, 31536000) > 1) {
    console.log(`${Math.floor(handleTimer(seconds, 31536000))} năm trước!`);
    return;
  }
  if (handleTimer(seconds, 2678400) > 1) {
    console.log(`${Math.floor(handleTimer(seconds, 2678400))} tháng trước!`);
    return;
  }
  if (handleTimer(seconds, 604800) > 1) {
    console.log(`${Math.floor(handleTimer(seconds, 604800))} tuần trước!`);
    return;
  }
  if (handleTimer(seconds, 86400) > 1) {
    console.log(`${Math.floor(handleTimer(seconds, 86400))} ngày trước!`);
    return;
  }
  if (handleTimer(seconds, 3600) > 1) {
    console.log(`${Math.floor(handleTimer(seconds, 3600))} giờ trước!`);
    return;
  }
  if (handleTimer(seconds, 60) > 1) {
    console.log(`${Math.floor(handleTimer(seconds, 60))} giây trước!`);
    return;
  }
  return;
}

timeSince(yourFriendTime);
