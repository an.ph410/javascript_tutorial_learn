# Chương 1. Biến và các dữ liệu thường gặp

- Giới thiệu
- JS là gì? 2 cách sử sụng JS vào HTML cơ bản
    Javascript là ngôn ngữ lập trình, chạy ở phía client, là ngôn ngữ thông dịch.
- console.log là gì? Tại sao lại sử dụng
    Dùng để log ra bên ngoài tab console của Chrome để dev xem giá trị
- Biến là gì? Tại sao phải dùng Biến
    Variable: dùng để lưu giá trị dữ liệu, sử dụng ở nhiều nơi
- Đặt tên hiệu quả với camelCase
    camelCase -> evondev -> evonDev
- Lỗi đặt tên cần phòng tránh
- Khai báo biến với const - let
    Declare: khai báo 
    const: hằng số, không được phép thay đổi
- Sự khác nhau const và let cần nắm vững
- Hoisting là cái gì? Vấn đề hoisting cần nắm?
    const, let: không bị hoisting
    var: sẽ hoisting
- Các kiểu dữ liệu thông dụng trong Javascript
    Data Types: Number, String, Boolean, Undefined, Null

# Chương 2. Các kiểu dữ liệu thông dụng

# Những thữ giúp tạo cảm hứng khi coding
1. https://vscodethemes.com/