console.log(
  "1/ Phương thức element.insertAdjacentText => nội dung truyền vào là 1 chuỗi"
);

const h3 = document.querySelector("h3");
console.log(`h3.insertAdjacentText(position, text)\n
position: beforebegin, afterbegin, beforeend, afterend
`);

h3.insertAdjacentText(
  "afterbegin",
  `Đây là thẻ h2: dung insertAdjacentText có position là afterbegin`
);

h3.insertAdjacentHTML(
  "afterend",
  `<h2>Đây là thẻ h2: dung insertAdjacentHTML có position là afterend</h2>`
);
console.log(
  "2/ Phương thức element.insertAdjacentElement => nội dung truyền vào là 1 chuỗi"
);

console.log(`element.insertAdjacentText(position, node)\n
position: beforebegin, afterbegin, beforeend, afterend
`);
const strong = document.createElement("strong");
strong.classList.add("bold");

h3.insertAdjacentElement("beforeend", strong);
console.log(
  "3/ Phương thức element.insertAdjacentHTML => là 1 template html"
);

console.log(`element.insertAdjacentHTML(position, template string)\n
position: beforebegin, afterbegin, beforeend, afterend
`);

const template = `
<ul class="menu">
    <li>1</li>
    <li>2</li>
    <li>3</li>
    <li>4</li>
</ul>
`;

document.body.insertAdjacentHTML("beforeend", template)