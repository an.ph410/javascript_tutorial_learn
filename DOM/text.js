console.log("1/ textContent");
const spinner = document.querySelector("#spinner");
// spinner.textContent = "Changed";
console.log(spinner.textContent);

console.log(
  "2/ innerText: loại bỏ khoảng trắng đầu cuối, nếu có display:none => ko lấy được"
);
console.log(spinner.innerText);

console.log("3/ innerHTML => lấy ra nội dung của selector bao gồm cả html");
spinner.innerHTML = `<div style="color: blue; font-weight: 700">innerHTML nè</div>`;

console.log(spinner.innerHTML);
