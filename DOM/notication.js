const lstImg = [
  "https://images.unsplash.com/photo-1576664839181-e08ac3624a6d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
  "https://images.unsplash.com/photo-1628872610206-cf9fec5f91d6?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
  "https://images.unsplash.com/photo-1522032238811-74bc59578599?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1074&q=80",
  "https://images.unsplash.com/photo-1606041008023-472dfb5e530f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=688&q=80",
  "https://images.unsplash.com/photo-1447875569765-2b3db822bec9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
  "https://images.unsplash.com/photo-1474112704314-8162b7749a90?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
  "https://images.unsplash.com/photo-1525547719571-a2d4ac8945e2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=764&q=80",
  "https://images.unsplash.com/photo-1498050108023-c5249f4df085?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1472&q=80",
  "https://images.unsplash.com/photo-1664798769882-ea0fd2254eb0?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
  "https://images.unsplash.com/photo-1664109478657-343a74b62ad0?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=736&q=80",
];

const data = [
  {
    title: "Welcome to javascript course",
    desc:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
  },
  {
    title: "Welcome to this tutorial",
    desc: "JavaScript is the world's most popular programming language. CSS describes how HTML elements should be displayed.",
  },
  {
    title: "Today is a good day",
    desc: "This tutorial will teach you JavaScript from basic to advanced.CSS is the language we use to style an HTML document.",
  },
  {
    title: "My name is An Pham",
    desc:
      "JavaScript is the programming language of the Web. JavaScript is easy to learn. This tutorial will teach you CSS from basic to advanced.",
  },
  {
    title: "I am frontend devdeloper",
    desc:
      "We recommend reading this tutorial, in the sequence listed in the menu.If you have a large screen, the menu will always be present on the left.",
  },
];

function createNoification(
  srcImg,
  {
    title = "Welcome to notification",
    desc = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
  }
) {
  const template = `
        <div class="noti">
            <img
                src=${srcImg}
                alt=""
                class="noti-image"
            />
            <div class="noti-content">
                <h3 class="noti-title">${title}</h3>
                <p class="noti-desc">${desc}</p>
            </div>
        </div>
    `;

  const body = document.body;
  body.insertAdjacentHTML("afterbegin", template);
}

function randomImg() {
  let index = Math.floor(Math.random() * 10);
  return lstImg[index];
}

function randomContent() {
  let index = Math.floor(Math.random() * data.length);
  return data[index];
}

const timer = setInterval(function () {
  const itemNoti = document.querySelector(".noti");
  if (itemNoti) {
    itemNoti.parentNode.removeChild(itemNoti);
  }
  createNoification(randomImg(), randomContent());
}, 4000);
