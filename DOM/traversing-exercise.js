// Active Modal

const template = `
<div class="modal">
      <div class="modal-content">
        <i class="fa fa-times modal-close"></i>
        <div class="modal-desc">
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry. Lorem Ipsum has been the industry's standard dummy text ever
          since the 1500s, when an unknown printer took a galley of type and
          scrambled it to make a type specimen book. It has survived not only
          five centuries, but also the leap into electronic typesetting,
          remaining essentially unchanged. It was popularised in the 1960s with
          the release of Letraset sheets containing Lorem Ipsum passages, and
          more recently with desktop publishing software like Aldus PageMaker
          including versions of Lorem Ipsum.
          <div class="modal-action">
            <button class="modal-submit">Confirm</button>
            <button class="modal-cancel">Cancel</button>
          </div>
        </div>
      </div>
    </div>
`;

const modal = document.createElement("div");
modal.classList.add("modal");

const modalContent = document.createElement("div");
modalContent.classList.add("modal-content");
modal.appendChild(modalContent);

const modalClose = document.createElement("i");
modalClose.className = "fa fa-times modal-close";
modalContent.appendChild(modalClose);

const modalDesc = document.createElement("div");
modalDesc.classList.add("modal-desc");
modalDesc.textContent = `
Lorem Ipsum is simply dummy text of the printing and typesetting
industry. Lorem Ipsum has been the industry's standard dummy text ever
since the 1500s, when an unknown printer took a galley of type and
scrambled it to make a type specimen book. It has survived not only
five centuries, but also the leap into electronic typesetting,
remaining essentially unchanged. It was popularised in the 1960s with
the release of Letraset sheets containing Lorem Ipsum passages, and
more recently with desktop publishing software like Aldus PageMaker
including versions of Lorem Ipsum.
`;
modalContent.appendChild(modalDesc);

const modalAction = document.createElement("div");
modalAction.classList.add("modal-action");
const btnConfirm = document.createElement("button");
const btnCancel = document.createElement("button");

btnConfirm.classList.add("modal-submit");
btnCancel.classList.add("modal-cancel");

btnConfirm.textContent = "Confirm";
btnCancel.textContent = "Cancel";

modalAction.appendChild(btnConfirm);
modalAction.appendChild(btnCancel);
modalDesc.appendChild(modalAction);

const body = document.body;
body.appendChild(modal);
// body.insertAdjacentHTML("afterbegin", template);
const modalWrapper = document.querySelector(".modal");

if (modalWrapper) {
  setTimeout(function () {
    modalWrapper.classList.add("is-show");
  }, 2000);
}
