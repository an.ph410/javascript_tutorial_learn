console.log(
  '1/ selector.getAttribute("attribute") => return giá trị của attribute selector, nếu không có => return null\n',
  "attribute (thuộc tính): href, style inline, src, class, id"
);

const link = document.querySelector(".link");

const attributeLink = link.getAttribute("href");

const li = document.querySelectorAll(".item");
li.forEach((item) => console.log(item.getAttribute("class")));

console.log(attributeLink);

console.log(
  '2/ selector.setAttribute("attribute", value) => set giá trị cho attribute nào đó của selector'
);

link.setAttribute("target", "_blank");
const lstLinks = document.querySelectorAll("a.link");
lstLinks.forEach((item) => item.setAttribute("target", "_blank"));

console.log(
  '3/ selector.removeAttribute("attribute") => loại bỏ attribute nào đó của selector'
);

const idSpinner = document.querySelector("p#spinner");
idSpinner.removeAttribute("style");
console.log(idSpinner);

console.log(
  '4/ selector.hasAttribute("attribute") => kiểm tra selector có attribute nào đó hay không => return true, false'
);

if (idSpinner.hasAttribute("id")) {
    console.log("có id");
}
