const div = document.createElement("div");

document.body.appendChild(div);

const span = document.querySelector("span");
console.log(span.parentNode.parentNode);
console.log(span.parentElement);

div.innerHTML = `
    1. parentNode: <br/>
    Ví dụ: span.parentNode.parentNode <br/>
    2. parentElement: <br/>
    Ví dụ: span.parentElement <br/>
    3. selector.parentNode hoặc selector.parentElement.removeChild(delete selector) <br/>
    Ví dụ: span.parentNode.removeChild(span) <br/>
    4. selector.nextElementSibling và selector.previousElementSibling<br/>
    Ví dụ: span.nextElementSibling <br/>
    5. selector.childNodes => trả về hết các node bên trong bao gồm textNodes<br/>
    6. selector.children => trả về hết các node bên trong không bao gồm textNodes<br/>
    7. selector.firstChild => trả về node đầu tiên không bao gồm textNodes<br/>
    8. selector.firstElementChild => trả về node đầu bên trong không bao gồm textNodes<br/>
    9. selector.nextSibling và selector.previousSibling<br/>
`;

// span.parentNode.removeChild(span);
const nextSpan = span.nextElementSibling;
console.log("nextElementSibling", nextSpan);
console.log(span.nextSibling);

const prevSpan = span.previousElementSibling;
console.log(prevSpan);
console.log(span.previousSibling);

console.log(span.childNodes); // text, strong, text
console.log(span.children); // strong

console.log(span.firstChild); // #text
console.log(span.firstElementChild); //strong

console.log(span.lastChild); // #text
console.log(span.lastElementChild); //strong
