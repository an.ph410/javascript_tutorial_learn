console.log("Các phương thức về class");

const divContainer = document.querySelector(".container");
console.log(divContainer);

console.log('1/ selector.classList.add("container")');
divContainer.classList.add("is-active");
console.log(divContainer);

console.log('2/ selector.classList.remove("container")');
const h1Title = document.querySelector("h1");
console.log(h1Title);
h1Title.classList.remove("is-point");
console.log(h1Title);

console.log('3/ selector.classList.contains("container")');
console.log(divContainer.classList.contains("is-active"));

console.log(
  '4/ selector.classList.toggle("is-active") => tức là nếu có class is-active rồi => remove class đó đi, ngược lại add vào'
);

h1Title.classList.toggle("is-active");
console.log(h1Title);

divContainer.classList.toggle("is-active");
console.log(divContainer);

console.log("5/ selector.className => trả ra chuỗi các class của selector");
const title = document.querySelector("h1");
console.log(title.className);
title.className = "title";
console.log(title);
