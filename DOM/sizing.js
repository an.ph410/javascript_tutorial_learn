const template = `
    <div>
        1. Các thuộc tính liên quan đến offset:
            <ul>
                <li>offsetWidth: width của selector</li>
                <li>offsetHeight: height của selector</li>
                <li>offsetLeft: vị trí của selector so với bên trái</li>
                <li>offsetParent: lấy ra phần tử cha để lấy giá trị của phần tử cha</li>
                <li>offsetTop: vị trí của selector so với phí trên = padding top của selector + offsetHeight của các element ở trên</li>
            </ul>
        2. Các thuộc tính liên quan đến client:
            <ul>
                <li>clientWidth: width của selector - border</li>
                <li>clientHeight: height của selector - border</li>
                <li>clientLeft: vị trí của selector so với bên trái border</li>
                <li>clientTop: vị trí của selector so với phí trên border</li>
            </ul>
        3. Các thuộc tính liên quan đến window:
            <ul>
                <li>window.innerWidth</li>
                <li>window.innerHeight</li>
                <li>window.outerWidth</li>
                <li>window.outerHeight</li>
            </ul>
        4. Tìm hiểu selector.getBoundingClientRect() => Lấy ra tọa độ, kích thước của phần tử:
            <ul>
                <li>left, x: vị trí của khối so với bên trái</li>
                <li>top: vị trí khối so với phía trên</li>
                <li>bottom: chiều cao của khối + top</li>
                <li>right: độ rộng của khối + left</li>
                <li>width: rộng</li>
                <li>height: cao</li>
            </ul>
    </div>
`;

const body = document.body;
body.insertAdjacentHTML("beforeend", template);

const boxed = document.querySelector(".boxed");
const h3 = document.querySelector("h3");
function log(value) {
  console.log(value);
}

log(boxed.offsetWidth);
log(boxed.offsetHeight);
log(boxed.offsetLeft);
log(boxed.offsetParent);
log(boxed.offsetTop);
log(h3.offsetHeight);

log(boxed.clientWidth);
log(boxed.clientHeight);
log(boxed.clientLeft);
log(boxed.clientTop);

log("------------window-----------")
log(window.innerWidth)
log(window.innerHeight)
log(window.outerWidth)
log(window.outerHeight)

log("------------getBoundingClientRect()-----------")
log(boxed.getBoundingClientRect())
