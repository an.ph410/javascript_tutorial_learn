console.log("Add, Remove, Edit Node in JS");
console.log(
  '1/ Tạo ra element trong javascript: document.createElement("tag")'
);

const div = document.createElement("div");

console.log("2/ selector.appendChild");
const body = document.body;
body.appendChild(div);

div.classList.add("container");
div.className = "container wrapper";

div.textContent = `Lorem Ipsum is simply dummy text of the printing and typesetting industry.
      Lorem Ipsum has been the industry's standard dummy text ever since the
      1500s, when an unknown printer took a galley of type and scrambled it to
      make a type specimen book. It has survived not only five centuries, but
      also the leap into electronic typesetting, remaining essentially
      unchanged. It was popularised in the 1960s with the release of Letraset
      sheets containing Lorem Ipsum passages, and more recently with desktop
      publishing software like Aldus PageMaker including versions of Lorem
      Ipsum.`;

div.innerHTML = `<div class="content"><h3>ABG</h3></div>`;
div.setAttribute("data-name", "blog");

const divCard = document.createElement("div");
body.appendChild(divCard);

divCard.classList.add("card");
const tagImg = document.createElement("img");
divCard.appendChild(tagImg);

tagImg.setAttribute(
  "src",
  "https://images.unsplash.com/photo-1539008835657-9e8e9680c956?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80"
);

tagImg.setAttribute("alt", " ");
tagImg.setAttribute("class", "card-image");

console.log("3/ document.createTextNode");
const text = document.createTextNode("Hello my name is An");
const h1 = document.createElement("h1");
h1.appendChild(text);
body.appendChild(h1);

console.log("4/ Phương thức element.cloneNode");

const h1Clone = h1.cloneNode(true);
const h2Clone = h1.cloneNode(false);
body.appendChild(h1Clone)
body.appendChild(h2Clone)

console.log("4/ Phương thức element.hasChildNodes => kiểm tra có phần tử con hay không");
console.log(h1Clone.hasChildNodes());
console.log(h2Clone.hasChildNodes());
