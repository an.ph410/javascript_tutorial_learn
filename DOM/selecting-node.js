console.log("1/ DOM là gì?");
console.log(
  "Document Oject Model\n",
  "DOM attribute: scr trong script\n",
  "DOM node: html, body, meta"
);

console.log("2/ Selecting Nodes");
console.log(
  '2.1/ document.querySelector("selector") => return a node nếu tồn tại, ngược lại return null'
);
console.log("selectors: .header, p, body, #heading");

const singleNode = document.querySelector("h1");
const singleNode2 = document.querySelector(".container");
const singleNode3 = document.querySelector("#spinner");

console.log(singleNode, singleNode2, singleNode3);

console.log(
  '2.2/ document.querySelectorAll("selector") => return a NodeList chưa list các node, ngược lại return empyt. \n Nó giống mảng nhưng ko phải mảng\n Có thể loop: forEach, for, for...of \n Còn các method khác thì không'
);

const multipleNode = document.querySelectorAll(".item");
const singleNode4 = document.querySelector(".item");

console.log(
  `Dùng querySelector => return về node đầu tiên thôi:\n`,
  singleNode4
);
console.log(`Dùng querySelectorAll => return về list node:\n`, multipleNode);

console.log(
  '2.3/ document.getElementsByClassName("className") => return a node HTMLCollection chứa list các node, nếu không có => return empty'
);

const classNodes = document.getElementsByClassName("container");
console.log(classNodes);

console.log(
  '2.4/ document.getElementsByTagName("tagName") => return a node HTMLCollection chứa list các node, nếu không có => return empty []'
);

const tagNodes = document.getElementsByTagName("li");
console.log(tagNodes);

console.log(
  '2.5/ document.getElementById("id") => return 1 node, nếu không có => return null'
);

const idNode = document.getElementById("spinner");
console.log(idNode);

console.log(
  '2.6/ getAttribute("id") => return 1 node, nếu không có => return null'
);