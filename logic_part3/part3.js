// Number(value)
console.log("4.5");
console.log("4");
console.log("abd");

// NaN: Not a number
console.log(Number(undefined)); //NaN
console.log(Number(null)); // 0
console.log(Number(false)); // 0
console.log(Number("")); // 0
console.log(Number(true)); // 1
console.log(Number(NaN)); // NaN


// String(value)
console.log(String(4.5)); // "4.5"

// Boolean(value)
// Falsy: 0, "", false, NaN, null, undefined, -0
console.log(Boolean(NaN));

// Type coerico: Nói đến vấn đề chuyển đổi kiểu dữ liệu
// Đối với phép + -> ưu tiên chuyển về string
// Đối với - * / % -> return number
console.log(null + ""); // "null"
console.log(null + undefined); // NaN
console.log("" - 1); // Number("") = 0 -> 0 - 1 = -1
console.log(false - true); // 0 - 1 => -1
console.log(null + 10); // Number(null) = 0 -> 10
 

// Toán tử so sánh cơ bản: > >= < <=
// Toán tử logic: && || !
// So sánh == (loose equality) và === (strict equality)
