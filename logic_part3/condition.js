// Câu điều kiện cơ bản if else
// alertm prompt confirm
// alert("Your website has been hacked")
// const yourName  = prompt("Vui long nhap ten cua ban", "Tuan") // nhập 1 giá trị
// console.log(yourName);

// const isYourLaptop = confirm("Is your laptop?", true)
// console.log(isYourLaptop);
// Bài tập câu điều kiện
// B1
// const yourAge = prompt("Your Age:");
// if (Number(yourAge) >= 18) {
//   console.log("Moi ban vao coi phim!");
// } else {
//   console.log("Ban chua du tuoi");
// }

// B2
function CompareNumber(a, b) {
  if (a > b) {
    alert(a);
  } else if (a < b) {
    alert(b);
  } else {
    alert("Hai so bang nhau");
  }
}

// CompareNumber(10, 5)

// switch ... case

// Ternary operator
// condition ? true : false
const age = 18;
let message = age >= 18 ? "You are adult" : "You are still a child";

console.log(message);

function main() {
    var depth = parseInt(readLine(), 10);
    let slipsBack = 2;
    let climbsUp = 7;
    let daysFinish = 0;
    let currentFeet = 0;

    while (currentFeet <= depth) {
        currentFeet = currentFeet + climbsUp - slipsBack;
        daysFinish++;
    }

    if (currentFeet - slipsBack > depth) {
        daysFinish = daysFinish - 1;
    }

    console.log(daysFinish)
}